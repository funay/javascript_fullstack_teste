export interface Chamado {
    id: string,
    id_paciente: string;
    nome_paciente: string;
    id_motivo: string;
    numero_chamado: string;
    descricao: string;
    status: string;
    data_criacao: Date;
}
