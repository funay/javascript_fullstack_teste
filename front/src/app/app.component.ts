import { Component, AfterViewInit, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Chamado } from "src/model/chamado";
import { ChamadosService } from "src/service/chamados.service";
import { Motivo } from "src/model/motivo";
import { MotivosService } from "src/service/motivo.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  title = "angularFullstackTeste";
  chamados: Chamado[];
  from: number = 0;
  size: number = 5;
  totalDocs: number = 0;
  editRowId: String;
  motivos: Motivo[];
  cacheChamado: Chamado;
  pageList: Array<number> = [];
  pagesToShow: Array<number> = [];
  maxPageToShow: number = 5;
  constructor(
    private chamadosService: ChamadosService,
    private motivosService: MotivosService
  ) {}
  async getChamadosPage() {
    const chamados$ = this.chamadosService.findAllChamados(
      this.from,
      this.size
    );
    return (this.chamados = (await chamados$.toPromise()).map((e) => e));
  }
  async ngOnInit() {
    this.motivos = await this.motivosService.getMotivos();
    await this.getChamadosPage();
    this.totalDocs = this.chamadosService.getChamadosLength();
    for (let i = 0; i * this.size < this.totalDocs; i++) this.pageList.push(i);
    this.getPagestoShow();
  }
  getPagestoShow() {
    let index = 0,
      pos = 0;
    this.pagesToShow = [];
    console.log(this.pageList.length);
    while (index + 1 <= this.maxPageToShow && index <= this.pageList.length) {
      if (this.from / this.size - pos >= 0) {
        this.pagesToShow.push(this.from / this.size - pos);
        // console.log('--', this.from/this.size - pos)
        index++;
      }
      if (
        pos != 0 &&
        this.from / this.size + index <= this.pageList.length &&
        index < this.maxPageToShow
      ) {
        this.pagesToShow.push(this.from / this.size + pos);
        // console.log('++', this.from/this.size + pos)
        index++;
      }
      pos++;
    }
    this.pagesToShow = this.pagesToShow.sort((a, b) => a - b);
  }
  addRow() {
    let newChamado: Chamado = {
      id: '',
      data_criacao: new Date(),
      descricao: '',
      id_motivo: '',
      id_paciente: '',
      nome_paciente: '',
      numero_chamado: '',
      status: '',
    };
    this.editRowId = '';
    this.cacheChamado = newChamado;
    this.chamados.push(newChamado);
  }

  async getNextPage(index) {
    this.from = this.size * index;
    await this.getChamadosPage();
    this.getPagestoShow();
  }
  checkPage(index) {
    console.log(index === this.from / this.size ? "active" : "");
    return index === this.from / this.size ? "active" : "";
  }
  getMotivoDescricao(id) {
    const motivo = this.motivos.find((e) => e.id_motivo == id);
    return motivo && motivo.descricao;
  }
  onChamadoChange(field, value) {
    this.cacheChamado[field] = value;
    console.log(this.cacheChamado);
  }
  async openCall(action, chamado) {
    const actions = {
      edit: (chamado) => {
        this.editRowId = chamado.id;
        this.cacheChamado = chamado;
      },
      save: async (chamado) => {
        (await chamado.id !== '')
          ? this.chamadosService.updateChamado(this.cacheChamado)
          : this.chamadosService.createChamado(this.cacheChamado);
        this.editRowId = null;
      },
      cancelar: () => {
        this.editRowId = null;
      },
    };
    await actions[action](chamado);
  }
}
