import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { ChamadosService } from 'src/service/chamados.service';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { MotivosService } from 'src/service/motivo.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: 'BASE_API_URL',
      useValue: environment.apiUrl
    },
    ChamadosService,
    MotivosService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
