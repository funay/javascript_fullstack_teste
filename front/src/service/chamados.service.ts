import { Injectable, Inject } from "@angular/core";
import {
  HttpClient,
  HttpParams,
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { Chamado } from "../model/chamado";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment.prod";

@Injectable()
export class ChamadosService implements HttpInterceptor {
  totalDocuments: number = 0;
  constructor(
    @Inject("BASE_API_URL") private baseUrl: string,
    private http: HttpClient
  ) {
    // private http: HttpClient) {
  }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const apiReq = request.clone({ url: `${this.baseUrl}/${request.url}` });
    return next.handle(apiReq);
  }

  findCourseById(courseId: number): Observable<Chamado> {
    return this.http.get<Chamado>(`/api/courses/${courseId}`);
  }
  getChamadosLength(): number {
    return this.totalDocuments;
  }
  updateChamado(chamado) {
    return this.http
      .put(`${environment.apiUrl}/chamado`, {
        _id: chamado.id,
        source: chamado,
      })
      .toPromise();
  }
  createChamado(chamado) {
    return this.http
      .post(`${environment.apiUrl}/chamado`, {
        ...chamado,
      })
      .toPromise();
  }
  findAllChamados(from, size): Observable<Chamado[]> {
    try {
      return this.http
        .get(`${environment.apiUrl}/chamado`, {
          params: {
            from,
            size,
          },
        })
        .pipe(
          map((res) => {
            this.totalDocuments = res["hits"]["total"]["value"];
            return res["hits"]["hits"].map((hits) => {
              return { ...hits._source, id: hits._id };
            });
          })
        );
    } catch (error) {
      return;
    }
  }
}
