import { Injectable, Inject } from "@angular/core";
import {
  HttpClient,
  HttpParams,
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { Motivo } from "src/model/motivo";
import { environment } from "src/environments/environment.prod";

@Injectable()
export class MotivosService implements HttpInterceptor {
  constructor(
    @Inject("BASE_API_URL") private baseUrl: string,
    private http: HttpClient
  ) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const apiReq = request.clone({ url: `${this.baseUrl}/${request.url}` });
    return next.handle(apiReq);
  }

  getMotivos(): Promise<Motivo[]> {
    return this.http.get<Motivo[]>(`${environment.apiUrl}/motivo`).toPromise();
  }
}
