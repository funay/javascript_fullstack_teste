Primeiramente, foi um prazer fazer parte do processo seletivo.

Infelizmente não deu tempo de terminar todos os requisitos descritos pela especificação do desafio (acredito que o tempo para implementação também faça parte do desafio).

então neste desafio tentei mostrar alguns conhecimentos: 
1) Verificar a consistência dos dados que estão sendo inseridos no modelo (fiz para o CPF mas poderia ter feito também para os outros)
2) Criação de uma tabela editável no angular 6 (Fiz manualmente mas normalmente uso o component pronto do angular 8)
3) Logica para paginação tanto na exibição do número de páginas de maneira circular, quanto na busca pelos chamados.
4) Não implementei a parte de busca (acredito que seja a parte mais demorada do front nesse desafio).
5) Não implementei a busca por pacientes para editar um chamado mas utilizei uma logica que seria parecida para consultar o motivo do chamado (exibindo a descrição porém salvando o id)
6) A parte de segurança não foi implementada, mas possuo experiência implementando jwt como método de autenticação (não implementei pois por ser um diferencial, foquei no que estava sendo pedido no desafio).

O dump do banco está no arquivo database.sql. Foi utilizado o Sync do sequelize para criar as tabelas pela aplicação também (apenas o banco precisa ser criado manualmente). Acredito que o ideal era que a população desse banco também fosse entregue por migrations.

No arquivo config.js é onde é setado as configurações de banco de dados

Collection usada no postman https://www.getpostman.com/collections/810a26eede864a6ee835