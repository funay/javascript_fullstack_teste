module.exports = () => ({
  port: 3081,
  redis: {
    password: "a4d0985e57073d9927ded659ed447e17fac33f99",
    port: 6379,
    host: 'homologacao.tapcore.com.br',
    db: 0,
  },
  sequelize: {
    "development": {
      "username": "root",
      "password": "password",
      "database": "database_development",
      "host": "127.0.0.1",
      "dialect": "mysql",
      "logging": true
    },
    "test": {
      "username": "root",
      "password": null,
      "database": "database_test",
      "host": "127.0.0.1",
      "dialect": "mysql"
    },
    "production": {
      "username": "root",
      "password": null,
      "database": "database_production",
      "host": "127.0.0.1",
      "dialect": "mysql",
      "logging": false,
    }
  },
  elasticSearch: {
    host: 'https://search-teste-fullstack-js-2v3vh2ph5iuthuvawkrfwpfwnq.sa-east-1.es.amazonaws.com/',
    index: 'ms_chamados_teste_classe2_4',
    type: 'chamados'
  }
});
