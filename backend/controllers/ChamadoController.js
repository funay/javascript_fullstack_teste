const uuid = require('uuid').v4;
const ChamadoController = (app) => {
  const { config } = app;
  const { elasticSearch } = app.database
  const createChamado = async (body) => elasticSearch.index({
    index: config.elasticSearch.index,
    type: config.elasticSearch.type,
    id: uuid(),
    body: {
      ...body,
      numero_chamado: body.numero_chamado || uuid(),
      data_criacao: body.data_criacao || new Date().toISOString().split('T')[0]
    }
  });
  const getChamado = ({ size, from }) => elasticSearch.search({
    index: config.elasticSearch.index,
    type: config.elasticSearch.type,
    body: {
      query: {
        match_all: {
          // status: 'status'
          // add params to search
        }
      },
    },

    size: size || 10,
    from: from || 0,
  })
  const updateChamado = (body) => elasticSearch.update({
    index: config.elasticSearch.index,
    type: config.elasticSearch.type,
    id: body._id,
    body: {
      doc: {
        ...body.source
      }
    },
  })

  return {
    get: getChamado,
    create: createChamado,
    update: updateChamado,
  };
};

module.exports = ChamadoController;
