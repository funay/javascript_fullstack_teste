const PlanoController = (app) => {
  const { planos } = app.database.sequelize.models;
  const findPlano = (params) => planos.findAll({ where: { ...params } });
  const createPlano = (params) => planos.create({ ...params });
  const deletePlano = (id) => planos.destroy({ where: { id } });
  const updatePlano = (body) => planos.update({ ...body }, { where: { id: body.id } });
  return Object.freeze({
    create: createPlano,
    find: findPlano,
    delete: deletePlano,
    update: updatePlano,
  })
};

module.exports = PlanoController;
