const PacienteController = (app) => {
  const { pacientes, planos } = app.database.sequelize.models;
  const findPaciente = (params) => pacientes.findAll({
    where: { ...params },
    include: [{
      model: planos,
    }]
  });
  const createPaciente = (params) => pacientes.create({ ...params });
  const deletePaciente = (id) => pacientes.destroy({ where: { id } });
  const updatePaciente = (body) => pacientes.update({ ...body }, { where: { id: body.id } });
  return Object.freeze({
    create: createPaciente,
    find: findPaciente,
    delete: deletePaciente,
    update: updatePaciente,
  })
};

module.exports = PacienteController;
