
const { DataTypes } = require('sequelize')
const uuid = require('uuid').v4;
const Pacientes = (app) => {
  const { isCpf } = app.utils.Validation
  const sequelizeInit = (sequelize) => {
    sequelize.define('pacientes', {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
      },
      cpf: {
        type: DataTypes.STRING,
        validate: {
          isCpf: (phone) => isCpf(phone)
        },
      },
      nome: DataTypes.STRING,
      data_nascimento: DataTypes.STRING,
      sexo: DataTypes.STRING,
      telefone: DataTypes.STRING,
    });
    sequelize.beforeCreate(paciente => paciente.id = uuid());
  }
  return {
    sequelizeInit,
  };
}

module.exports = Pacientes;