const { Sequelize, Model, DataTypes } = require('sequelize');
const sequelize = new Sequelize('sqlite::memory:');
const uuid = require('uuid').v4;
const Planos = () => {
  const sequelizeInit = (sequelize) => {
    sequelize.define('planos', {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
      },
      descricao: DataTypes.STRING
    });
    Sequelize.beforeCreate(plano => plano.id = uuid());
  }
  return {
    sequelizeInit,
  };
}

module.exports = Planos;