const { Sequelize } = require('sequelize');
let sequelize = null;
module.exports = (app) => {
  const { config, models } = app;
  if (!sequelize) sequelize = new Sequelize(config.sequelize[process.env.enviroment || 'development']);
  const _defineModels = (sequelize) => Object.keys(models).forEach(key => models[key].sequelizeInit && models[key].sequelizeInit(sequelize))
  const _applyDependences = (sequelize) => {
    const { pacientes, planos } = sequelize.models;
    pacientes.belongsTo(planos, { foreignKey: 'id_plano', targetKey: 'id' });
  }
  _defineModels(sequelize)
  _applyDependences(sequelize);
  sequelize.sync({ force: app.config.sequelize.force || false });
  return sequelize

};