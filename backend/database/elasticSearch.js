const { Client } = require('elasticsearch');

let elasticSearch = null;
module.exports = (app) => {
    const { config } = app;
    if (!elasticSearch) {
        elasticSearch = new Client({ ...config.elasticSearch });
    }
    return elasticSearch;
};


