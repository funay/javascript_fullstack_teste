module.exports = (app) => {
  const { ChamadoController } = app.controllers;

  /**
   * Lista os Chamados do chamado.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const insereChamado = async (req, res) => {
    try {
      return res.status(200).json(await ChamadoController.create(req.body));
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'Ocorreu uma falha interna no servidor.' });
    }
  };
  const consultaChamado = async (req, res) => {
    try {
      const chamados = await ChamadoController.get(req.query);
      res.status(200).json(chamados);
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'Ocorreu uma falha interna no servidor.' })
    }
  }
  const alteraChamado = async (req, res) => {
    try {
      const updatedChamado = await ChamadoController.update(req.body);
      res.status(200).json(updatedChamado);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Ocorreu uma falha interna no servidor.' })
    }
  }
  app.route('/Chamado')
    .get(consultaChamado)
    .post(insereChamado)
    .put(alteraChamado)
};
