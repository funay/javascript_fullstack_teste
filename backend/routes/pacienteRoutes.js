module.exports = (app) => {
  const { PacienteController } = app.controllers;

  /**
   * Lista os motivos do chamado.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const findPacientes = async (req, res) => {
    try {
      const pacientes = await PacienteController.find(req.query);
      return res.status(200).json(pacientes);
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  const createPaciente = async (req, res) => {
    try {
      const createdPaciente = await PacienteController.create(req.body);
      return res.status(200).json(createdPaciente);
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  const updatePacientes = async (req, res) => {
    try {
      const updatedpacientes = await PacienteController.update(req.body);
      return res.status(200).json(updatedpacientes)
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Ocorreu uma falha interna no servidor.' });
    }
  }
  const deletePacientes = async (req, res) => {
    try {
      if (!req.query.id) throw 'Id param is required to delete';
      console.log(req.query.id)
      const updatedpacientes = await PacienteController.delete(req.query.id);
      return res.status(200).json(updatedpacientes)
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Ocorreu uma falha interna no servidor.' });
    }
  }
  app.route('/paciente')
    .get(findPacientes)
    .post(createPaciente)
    .put(updatePacientes)
    .delete(deletePacientes)
};
