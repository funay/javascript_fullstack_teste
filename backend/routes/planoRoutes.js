module.exports = (app) => {
  const { PlanoController } = app.controllers;

  /**
   * Lista os motivos do chamado.
   * @param req
   * @param res
   * @returns {Promise<this>}
   */
  const findPlanos = async (req, res) => {
    try {
      const planos = await PlanoController.find(req.query);
      return res.status(200).json(planos);
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  const createPlano = async (req, res) => {
    try {
      const createdPlano = await PlanoController.create(req.body);
      return res.status(200).json(createdPlano);
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'Ocorreu uma falha interna no servidor.' });
    }
  };

  const updatePlanos = async (req, res) => {
    try {
      const updatedPlanos = await PlanoController.update(req.body);
      return res.status(200).json(updatedPlanos)
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Ocorreu uma falha interna no servidor.' });
    }
  }
  const deletePlanos = async (req, res) => {
    try {
      if (!req.query.id) throw 'Id param is required to delete';
      console.log(req.query.id)
      const updatedPlanos = await PlanoController.delete(req.query.id);
      return res.status(200).json(updatedPlanos)
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Ocorreu uma falha interna no servidor.' });
    }
  }
  app.route('/plano')
    .get(findPlanos)
    .post(createPlano)
    .put(updatePlanos)
    .delete(deletePlanos)
};
