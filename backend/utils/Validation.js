const CPF = require('cpf');
const Validation = () => {
  const isCpf = (cpf) => CPF.isValid(cpf);
  return {
    isCpf,
  }
}

module.exports = Validation;
